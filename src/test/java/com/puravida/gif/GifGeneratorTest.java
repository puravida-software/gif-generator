package com.puravida.gif;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GifGeneratorTest {

    @Test
    public void aSimpleTest() throws IOException {
        List<File> files = new ArrayList<File>();
        files.add( new File("src/test/resources/image1.jpg"));
        files.add( new File("src/test/resources/image2.jpg"));
        File output = GifGenerator.compose(files);
        System.out.println(output.getAbsolutePath());
    }

}
