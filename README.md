# gif-generator

Librería para construir un Gif partiendo de una secuencia de imágenes.

## coordenadas Maven

<dependency>
  <groupId>com.puravida-software</groupId>
  <artifactId>gif-generator</artifactId>
  <version>1.3.0</version>
</dependency>

## Uso

    List<File> files = new ArrayList<File>();
    files.add( new File("src/test/resources/image1.jpg"));
    files.add( new File("src/test/resources/image2.jpg"));
    
    // crear un gif con 1seg de timeframe y modo loop
    File gif1 = GigGenerator.compose(files);
    
    // crear un gif con 2seg de timeframe y modo loop desactivado
    File gif2 = GigGenerator.compose(files, 2000, false);
    
## File o byte[]

Las imágenes pueden ser proporcionadas como lista de ficheros o como lista
de byte arrays

## Normalización

La librería normaliza las imágenes al tamaño de la primera proporcionada

        
